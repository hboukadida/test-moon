import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Link, Navigate, Route, Router, Routes } from 'react-router-dom';
import HomeScreen from './screens/HomeScreen';
import ProductScreen from './screens/ProductScreen';
import SigninScreen from './screens/SigninScreen';
import PacksScreen from './screens/PacksScreen';
 import NavBar from './screens/NavBar';
import ProductsScreen from './screens/ProductsScreen';
import UsersScreen from './screens/UsersScreen';
import 'bootstrap/dist/css/bootstrap.min.css';
import AddUser from './screens/AddUser';
import RegisterScreen from './screens/RegisterScreen';
import ProtectedRoute from './ProtectedRoute';
import { useSelector } from 'react-redux';
import AddProductScreen from './screens/AddProductScreen';
import AddPackScreen from './screens/AddPackScreen';
import UpdateProductScreen from './screens/UpdateProductScreen';
import UpdatePackScreen from './screens/UpdatePackScreen';
import UpdateUserScreen from './screens/UpdateUserScreen';
import ListPacksScreen from './screens/ListPacksScreen';
import PackDetailsScreen from './screens/PackDetailsScreen';
import Navbar from './components/Navbar';
import "bootstrap/dist/css/bootstrap.min.css";


import { useState } from 'react';

function App() {

  const [sidebarIsOpen, setSidebarOpen] = useState(true);
  const toggleSidebar = () => setSidebarOpen(!sidebarIsOpen);
  const userSignin = useSelector(state => state.userSignin);
  const { userInfo } = userSignin;
  return (
    <div className="grid-container">
      <Navbar />
    
    
      <main className="main">
        <div className="content">
          <Routes>
      
            {
              !userInfo && (
                <>
                  <Route exact path='/login' element={< SigninScreen />}></Route>
                  <Route exact path='/register' element={< RegisterScreen />}></Route>
                </>
              )
            }


            {
              userInfo && userInfo.role_id === 3 ? (
                <>

                  <Route exact path='/' element={< HomeScreen />}></Route>
                  <Route exact path='/products' element={< ProductsScreen />}></Route>
                  <Route exact path='/listProducts' element={< ProductScreen />}></Route>

                  <Route exact path='/packs' element={< PacksScreen />}></Route>
                  <Route exact path='/users' element={< UsersScreen />}></Route>
                  <Route exact path='/addUser' element={< AddUser />}></Route>
                  <Route exact path='/addProduct' element={< AddProductScreen />}></Route>
                  <Route exact path='/addPack' element={< AddPackScreen />}></Route>
                  <Route exact path='/updateProduct/:id' element={< UpdateProductScreen />}></Route>
                  <Route exact path='/updatePack/:id' element={< UpdatePackScreen />}></Route>
                  <Route exact path='/updateUser/:id' element={< UpdateUserScreen />}></Route>
                  <Route exact path='/listPackes' element={< ListPacksScreen />}></Route>
                  <Route exact path='/pack/:id' element={< PackDetailsScreen />}></Route>




                </>
              ) : userInfo && userInfo.role_id === 2 ? (<>
                <Route exact path='/' element={< HomeScreen />}></Route>
                <Route exact path='/listProducts' element={< ProductScreen />}></Route>

                <Route exact path='/products' element={< ProductsScreen />}></Route>
                <Route exact path='/packs' element={< PacksScreen />}></Route>
                <Route exact path='/users' element={< UsersScreen />}></Route>

                <Route exact path='/updateProduct/:id' element={< UpdateProductScreen />}></Route>

                <Route exact path='/listPackes/' element={< ListPacksScreen />}></Route>
                <Route exact path='/pack/:id' element={< PackDetailsScreen />}></Route>
              </>
              ) : userInfo && userInfo.role_id === 1 ? ( <>
                <Route exact path='/' element={< HomeScreen />}></Route>
                <Route exact path='/listProducts' element={< ProductScreen />}></Route>

                <Route exact path='/listPackes/' element={< ListPacksScreen />}></Route>
                <Route exact path='/pack/:id' element={< PackDetailsScreen />}></Route>

              </>
              ) : null
            }


            <Route path="*" element={<Navigate to={userInfo ? "/" : "/login"} />} />
          </Routes>

        </div>

      </main>

    </div>
  );
}

export default App;
