import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import {
  productDeleteReducer,
  productListReducer, productSaveReducer,

} from './reducers/productReducers';
import {
    userListReducer,
    userDeleteReducer,
    userSaveReducer,
  } from './reducers/userReducers';
import thunk from 'redux-thunk';

import {
    userSigninReducer,
    userRegisterReducer,
  
  } from './reducers/userReducers';
  import Cookie from 'js-cookie';
import {  packDeleteReducer, packListReducer, packSaveReducer } from './reducers/packReducers';

   const userInfo = JSON.parse(localStorage.getItem('userInfo')) || null;
const initialState = {
    userSignin: { userInfo },
    userRegister:{userInfo}
};
const reducer = combineReducers({
  productList: productListReducer,
  userSignin: userSigninReducer,
  userList:userListReducer,
  userDelete:userDeleteReducer,
  userSave:userSaveReducer,
  userRegister: userRegisterReducer,
  productSave: productSaveReducer,
  productDelete: productDeleteReducer,

  packList: packListReducer,
  packDelete: packDeleteReducer,
  packSave:packSaveReducer,

});
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  reducer,
  initialState,
  composeEnhancer(applyMiddleware(thunk))
 
);
export default store;