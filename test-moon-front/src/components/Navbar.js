import React, { useState } from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import { Link } from 'react-router-dom';
import { SidebarDataAdministrator,SidebarDataAgent,SidebarDataClient } from './SidebarData';
import './Navbar.css';
import { IconContext } from 'react-icons';
import { useSelector } from 'react-redux';

function Navbar() {
    const [sidebar, setSidebar] = useState(false);
    const userSignin = useSelector(state => state.userSignin);
    const { userInfo } = userSignin;


    const showSidebar = () => setSidebar(!sidebar);

    const logout = () => {
        localStorage.clear();
        window.location.pathname = '/login'
      }

    return (
        <>
            <IconContext.Provider value={{ color: '#fff' }}>

                <nav className="navbar">
                    <div className="container-fluid">
                        <Link to='#' className='menu-bars'>
                            <FaIcons.FaBars onClick={showSidebar} />
                        </Link>

                        <div className="d-flex">
                            {userInfo ? (
                                <a href='#' className='navbar-text ' onClick={logout}>Logout</a>

                            ) : (
                                <>
                                    <Link to="/login" className='navbar-text'>Sign In</Link>
                                    <Link to="/register" className='navbar-text' >Register</Link>
                                </>

                            )}
                        </div>
                    </div>
                </nav>






                {userInfo ? (
                <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
                    <ul className='nav-menu-items' onClick={showSidebar}>
                        <li className='navbar-toggle'>
                            <Link to='#' className='menu-bars'>
                                <AiIcons.AiOutlineClose />
                            </Link>
                        </li>
                        {userInfo && userInfo.role_id === 3 ? (
                        <>
                        {SidebarDataAdministrator.map((item, index) => {
                            return (
                                <li key={index} className={item.cName}>
                                    <Link to={item.path}>
                                        {item.icon}
                                        <span>{item.title}</span>
                                    </Link>
                                </li>
                            );
                        })}

                        </>
                         ) : userInfo && userInfo.role_id === 2 ? (
                            <>
                              {SidebarDataAgent.map((item, index) => {
                            return (
                                <li key={index} className={item.cName}>
                                    <Link to={item.path}>
                                        {item.icon}
                                        <span>{item.title}</span>
                                    </Link>
                                </li>
                            );
                        })}
                            </>
                            ): <>
                                {SidebarDataClient.map((item, index) => {
                            return (
                                <li key={index} className={item.cName}>
                                    <Link to={item.path}>
                                        {item.icon}
                                        <span>{item.title}</span>
                                    </Link>
                                </li>
                            );
                        })}
                        </>}
                    </ul>
                </nav>
                ) : null}
            </IconContext.Provider>
        </>
    );
}

export default Navbar;