import React from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';

export const SidebarDataAdministrator = [
  {
    title: 'Home',
    path: '/',
    icon: <AiIcons.AiFillHome />,
    cName: 'nav-text'
  },
  {
    title: 'Products',
    path: '/listProducts',
    icon: <FaIcons.FaProductHunt />,
    cName: 'nav-text'
  },
  {
    title: 'Packs',
    path: '/listPackes',
    icon: <IoIcons.IoIosPaper />,
    cName: 'nav-text'
  },
  {
    title: 'Product Mangement',
    path: '/products',
    icon: <FaIcons.FaCartPlus />,
    cName: 'nav-text'
  },
  {
    title: 'Packes Mangement',
    path: '/packs',
    icon: <FaIcons.FaEnvelopeOpenText />,
    cName: 'nav-text'
  },
  {
    title: 'Users Mangement ',
    path: '/users',
    icon: <IoIcons.IoMdPeople />,

    cName: 'nav-text'
  },
];


export const SidebarDataAgent = [
  {
    title: 'Home',
    path: '/',
    icon: <AiIcons.AiFillHome />,
    cName: 'nav-text'
  },
  {
    title: 'Products',
    path: '/listProducts',
    icon: <FaIcons.FaProductHunt />,
    cName: 'nav-text'
  },
  {
    title: 'Packs',
    path: '/listPackes',
    icon: <IoIcons.IoIosPaper />,
    cName: 'nav-text'
  },
  {
    title: 'Product Mangement',
    path: '/products',
    icon: <FaIcons.FaCartPlus />,
    cName: 'nav-text'
  }

];


export const SidebarDataClient = [
  {
    title: 'Home',
    path: '/',
    icon: <AiIcons.AiFillHome />,
    cName: 'nav-text'
  },
  {
    title: 'Products',
    path: '/listProducts',
    icon: <FaIcons.FaProductHunt />,
    cName: 'nav-text'
  },
  {
    title: 'Packs',
    path: '/listPackes',
    icon: <IoIcons.IoIosPaper />,
    cName: 'nav-text'
  }

  

];