import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import {
  saveProduct,

} from '../actions/productActions';
import { useForm } from "react-hook-form";

function UpdateProductScreen() {

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors }
  } = useForm();

  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");
  const [image, setFile] = useState("");
  const [pack_id, setSelected] = useState("");
  const [data, setData] = useState([]);

  const productSave = useSelector((state) => state.productSave);
  const {
    loading: loadingSave,
    success: successSave,
    error: errorSave,
  } = productSave;

  function onChange(event) {

    const pack_id = event.target.value;
    setSelected(pack_id);
  }
  let { id } = useParams();

  const dispatch = useDispatch();
  const navigate = useNavigate();
  useEffect(() => {


    getProduct();
    getPacks();
    if (successSave) {
      window.location.pathname = '/products'
    }
  }, [successSave])

  async function getProduct() {
    let result = await fetch('http://127.0.0.1:8000/api/getProduct/' + id);
    result = await result.json();


    setFile(result.image);

    setSelected(result.pack_id);
    setValue('name', result.name)
    setValue('description', result.description)
    setValue('price', result.price)


  }


  async function getPacks() {
    let result = await fetch('http://127.0.0.1:8000/api/getPackAll');
    result = await result.json();
    setData(result);
  }
  const submitHandler = (data) => {

    const bodyFormData = new FormData();
    bodyFormData.append('id', id);

    bodyFormData.append('image', data.file[0]);
    bodyFormData.append('price', data.price);
    bodyFormData.append('pack_id', pack_id);

    bodyFormData.append('name', data.name);

    bodyFormData.append('description', data.description);


    dispatch(
      saveProduct(bodyFormData)
    );
  };


  return (
    <>
      <div className='container'>


        <div className='row'>
          <div className="col-sm-6 offset-sm-3 mt-5">
            <h1>Update product </h1>
            <form onSubmit={handleSubmit(submitHandler)}>
              <label>Name</label>

              <input type="text" onChange={(e) => setName(e.target.value)} className="form-control" placeholder="name" {...register("name", { required: true })}></input>
              {errors.name && <p className='text-danger'>This field is required</p>}

              <br />
              <label>Description</label>

              <textarea class="form-control" rows={3} onChange={(e) => setDescription(e.target.value)} className="form-control" placeholder="description" {...register("description", { required: true })}></textarea>
              {errors.description && <p className='text-danger'>This field is required</p>}

              <br />
              <img src={"http://127.0.0.1:8000/" + image}  alt="product" className='card-img-top' height={"250px"} width={'100px'}/>
              <br/>
              <label>Image</label>

              <input type="file" onChange={(e) => setFile(e.target.files[0])} className="form-control" {...register("file")}></input>

              <br />
              <label>Price</label>

              <input type="text" onChange={(e) => setPrice(e.target.value)} className="form-control" placeholder="price" {...register("price", { required: true })}></input>
              {errors.price && <p className='text-danger'>This field is required</p>}

              <br />
              <label>Pack</label>

              <select className="form-select" value={pack_id} onChange={onChange}>
                {
                  data.map((item, key) =>
                    <option value={item.id}>{item.name}</option>
                  )
                }


              </select>

              <br />
              <button type="submit" className="btn btn-lg btn-success"> Update</button>
            </form>
          </div>
        </div>
      </div>
    </>

  )

}

export default UpdateProductScreen