
import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Badge, Button, Card, Carousel, Container, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { listProducts } from '../actions/productActions';
import Modal from '../components/modal/Modal';

function HomeScreen(props) {

  const [modal, setModal] = useState(false);
  const [data, setData] = useState([]);
  const productList = useSelector((state) => state.productList);
  const { products, loading, error } = productList;
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(listProducts());
    getproduts();
    return () => {
      //
    };
  }, []);


  async function getproduts() {
    let result = await fetch('http://127.0.0.1:8000/api/getProductHome');
    result = await result.json();
    console.log(result);
    setData(result)
  }

  const getData = (product) => {

    return setModal(true);
  }

  return (

    <>

      {loading ? (
        <div>Loading...</div>
      ) : error ? (
        <div>{error}</div>
      ) : (


        <>
          <div className='container '>
            <div className='buttons d-flex justify-content-center mb-5 pb-5 mt-5'>
              <h1>Test-Moon</h1>
            </div>

          </div>
          <div className='row '>
            <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12 mr-5 ml-5'>
            <Carousel fade>
              <Carousel.Item>
                <img
                  className="d-block w-100 card-img-top"
                  src="/images/iphone-13.jpeg"
                  alt="First slide"
                  height={"250px"}
                />
                <Carousel.Caption>
                  <h3>Iphone 13 pro</h3>
                </Carousel.Caption>
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100 card-img-top"
                  src="/images/samsung-s-21.jpg"
                  alt="First slide"
                  height={"250px"}
                />

                <Carousel.Caption>
                  <h3>Samsung S 21</h3>
                </Carousel.Caption>
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100 card-img-top"
                  src="/images/mi-11-ultra-1024x505.png"
                  alt="First slide"
                  height={"250px"}
                />

                <Carousel.Caption>
                  <h3>Mi-11-ultra</h3>
                </Carousel.Caption>
              </Carousel.Item>
            </Carousel>
            </div>
          </div>
          <div className='row mt-2'>
            <p class="text-center"><h1>Last Product</h1></p>
          </div>
          { data.length>0 ? (
               <div className="container">

               <div className='row'>
   
                 {data.map(product => {
   
   
                   return (
                     <>
                       <div className='col-md-3 mb-4'>
                         <div className='card h-100 text-center p-4'>
                           <img variant="top" src={"http://127.0.0.1:8000/" + product.image} alt="product" className='card-img-top' height={"250px"} />
                           <Card.Body>
                             <h5 className="card-title mb-0">{product.name}</h5>
                             <p className="card-text lead fw-bold ">
                               {product.price} DT
                             </p>
                           </Card.Body>
                         </div>
                       </div>
                     </>
                   )
                 }
   
                 )
   
                 }
               </div>
   
             </div>
   
          ) : (
            <div className='row mt-2'>
            <p class="text-center"><h1>No Product Found</h1></p>
          </div>
          )
             
          }
       
        </>
      )}

    </>
  );
}
export default HomeScreen;