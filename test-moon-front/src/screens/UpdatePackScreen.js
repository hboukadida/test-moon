import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import {
  savePack,

} from '../actions/packActions';
import { useForm } from "react-hook-form";

function UpdatePackScreen() {

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors }
  } = useForm();

  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");
  const [image, setFile] = useState("");
  let { id } = useParams();

  const packSave = useSelector((state) => state.packSave);
  const {
    loading: loadingSave,
    success: successSave,
    error: errorSave,
  } = packSave;


  const dispatch = useDispatch();
  const navigate = useNavigate();
  useEffect(() => {
    getPack();
    if (successSave) {
      window.location.pathname = '/packs'
    }

  }, [successSave])

  async function getPack() {
    let result = await fetch('http://127.0.0.1:8000/api/getPack/' + id);
    result = await result.json();
 
    setFile(result.image);

    setValue('name', result.name)
    setValue('description', result.description)
    setValue('price', result.price)
  }

  const submitHandler = (data) => {

    const bodyFormData = new FormData();
    bodyFormData.append('id', id);

    bodyFormData.append('image', data.file[0]);
    bodyFormData.append('price', data.price);

    bodyFormData.append('name', data.name);

    bodyFormData.append('description', data.description);


    console.log(data)
    dispatch(
      savePack(bodyFormData)
    );
  };


  return (
    <>
      <div className='container'>


        <div className='row'>
          <div className="col-sm-6 offset-sm-3 mt-5">
            <h1>Update pack</h1>
            <form onSubmit={handleSubmit(submitHandler)}>
              <label>Name</label>
              <input type="text" {...register("name", { required: true })} onChange={(e) => setName(e.target.value)} className="form-control" placeholder="name"></input>
              {errors.name && <p className='text-danger'>This field is required</p>}

              <br />
              <label>Description</label>
              <textarea class="form-control" {...register("description", { required: true })} onChange={(e) => setDescription(e.target.value)} className="form-control" placeholder="description" rows="3"></textarea>
              {errors.description && <p className='text-danger'>This field is required</p>}

              <br />
              <img alt="product" className='card-img-top' height={"250px"} width={'100px'} src={"http://127.0.0.1:8000/" + image} />
              <br />
              <label>Image</label>

              <input type="file"  {...register("file")} onChange={(e) => setFile(e.target.files[0])} className="form-control"></input>

              <br />
              <label>Price</label>

              <input type="text" {...register("price", { required: true })} onChange={(e) => setPrice(e.target.value)} className="form-control" placeholder="price"></input>
              {errors.price && <p className='text-danger'>This field is required</p>}


              <br />
              <button type="submit" className="btn btn-lg btn-success"> Update</button>
            </form>
          </div>
        </div>
      </div>
    </>

  )

}

export default UpdatePackScreen