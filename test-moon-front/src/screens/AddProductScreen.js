import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useForm } from "react-hook-form";

import {
  saveProduct,

} from '../actions/productActions';

function AddProductScreen() {
  const {
    register,
    handleSubmit,

    formState: { errors }
  } = useForm();

  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");
  const [image, setFile] = useState("");
  const [role_id, setSelected] = useState("2");
  const [packs, setData] = useState([]);

  const productSave = useSelector((state) => state.productSave);
  const {
    loading: loadingSave,
    success: successSave,
    error: errorSave,
  } = productSave;

  function onChange(event) {

    const role_id = event.target.value;
    setSelected(role_id);
  }
  const dispatch = useDispatch();
  const navigate = useNavigate();
  useEffect(() => {
    getPacks();
    if (successSave) {
      window.location.pathname='/products'
      //navigate('/products')
    }

  }, [successSave])

  const submitHandler = (data) => {

    const bodyFormData = new FormData();
    bodyFormData.append('image', data.file[0]);
    bodyFormData.append('price', data.price);

    bodyFormData.append('name', data.name);

    bodyFormData.append('description', data.description);
    bodyFormData.append('pack_id', data.pack_id);

   
    dispatch(
      saveProduct(bodyFormData)
    );
  };

  async function getPacks() {
    let result = await fetch('http://127.0.0.1:8000/api/getPackAll');
            result = await result.json();
          setData(result);
}


  return (
    <>
      <div className='container'>


        <div className='row'>
          <div className="col-sm-6 offset-sm-3 mt-5">
            <h1>Add New product</h1>
            <form onSubmit={handleSubmit(submitHandler)}>
              <label>Name</label>

              <input type="text" onChange={(e) => setName(e.target.value)} className="form-control" placeholder="name" {...register("name", { required: true })}></input>
              {errors.name && <p className='text-danger'>This field is required</p>}

              <br />
              <label>Description</label>

              <textarea className="form-control" rows={3} onChange={(e) => setDescription(e.target.value)} className="form-control" placeholder="description"{...register("description", { required: true })}></textarea>
              {errors.description && <p className='text-danger'>This field is required</p>}

              <br />
              <label>Image</label>

              <input type="file" onChange={(e) => setFile(e.target.files[0])} className="form-control" {...register("file", { required: true })}></input>
              {errors.file && <p className='text-danger'>This field is required</p>}

              <br />
              <label>Price</label>

              <input type="text" onChange={(e) => setPrice(e.target.value)} className="form-control" placeholder="price" {...register("price", { required: true })}></input>
              {errors.price && <p className='text-danger'>This field is required</p>}

              <br />

              <label>Pack</label>

              <select className="form-select" {...register("pack_id")}  onChange={onChange}>
              <option value="">Please select pack</option>
                {
                  packs.map((item, key) =>
                    <option value={item.id}>{item.name}</option>
                  )
                }


              </select>
              <br />
              <button type="submit" className="btn btn-lg btn-success"> ADD</button>
            </form>
          </div>
        </div>
      </div>
    </>

  )

}

export default AddProductScreen