
import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Button, Container, Row, Table } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { deletePack, listPacks } from '../actions/packActions';
import swal from 'sweetalert2';

function PacksScreen(props) {


    const packList = useSelector((state) => state.packList);
    const { packs, loading, error } = packList;
    const dispatch = useDispatch();
    const [data, setData] = useState([]);
    const packDelete = useSelector((state) => state.packDelete);
    const {
        loading: loadingDelete,
        success: successDelete,
        error: errorDelete,
    } = packDelete;


    useEffect(async () => {
        getUsers();
        if (successDelete) {
            // dispatch(listPacks());
            getUsers();
        }

    }, [successDelete])



    async function getUsers() {
        let result = await fetch('http://127.0.0.1:8000/api/getPackAll');
        result = await result.json();
        setData(result)
    }


    const deleteHandler = (item) => {

        swal.fire({
            title: 'Are you sure to delete this record?',
            showDenyButton: true,
            confirmButtonText: 'Save',
            icon: 'warning'
        }).then((result) => {
            if (result.isConfirmed) {
                dispatch(deletePack(item.id));

                swal.fire('Saved!', '', 'success')
            }
        })

    };
    return (

        <>

            {loading ? (
                <div>Loading...</div>
            ) : error ? (
                <div>{error}</div>
            ) : (
                <Container>
                    <Row>
                        <div className='mt-5 mb-5'>  <h1>List Of Pack</h1>  <Link to="/addPack" className="btn btn-success btn-lg float-right">Add Pack</Link>
                        </div>

                    </Row>
                    {data.length >0 ? (<Row>
                        <Table striped bordered hover responsive>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th style={{ textAlign: "center" }}>Name</th>
                                    <th style={{ textAlign: "center" }}>image</th>
                                    <th style={{ textAlign: "center" }}>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    data.map((item, key) =>
                                        <tr key={item.id}>
                                            <td>{key + 1}</td>
                                            <td style={{ textAlign: "center" }}>{item.name}</td>
                                            <td style={{ textAlign: "center" }}>  <img className="img-thumbnail" width={"200px"} height={"200px"} src={"http://127.0.0.1:8000/" + item.image} /></td>

                                            <td style={{ textAlign: "center" }}>
                                                <Link to={"/updatePack/" + item.id} className="btn btn-primary btn-lg float-right">Update</Link>
                                                {' '}
                                                <button
                                                    className="btn btn-lg btn-danger"
                                                    onClick={() => { deleteHandler(item) }}
                                                >
                                                    Delete
                                                </button>
                                            </td>
                                        </tr>

                                    )
                                }


                            </tbody>
                        </Table>
                    </Row>) : (<div className='row mt-2'>
                        <p class="text-center"><h1>No Pack Found</h1></p>
                    </div>)}

                </Container>
            )}
        </>
    );
}
export default PacksScreen;