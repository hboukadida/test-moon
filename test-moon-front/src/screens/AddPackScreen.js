import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useForm } from "react-hook-form";

import {
  savePack,

} from '../actions/packActions';

function AddPackScreen() {
  const {
    register,
    handleSubmit,

    formState: { errors }
  } = useForm();

  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [description, setDescription] = useState("");
  const [image, setFile] = useState("");
  const [role_id, setSelected] = useState("2");

  const packSave = useSelector((state) => state.packSave);
  const {
    loading: loadingSave,
    success: successSave,
    error: errorSave,
  } = packSave;

  function onChange(event) {

    const role_id = event.target.value;
    setSelected(role_id);
  }
  const dispatch = useDispatch();
  const navigate = useNavigate();
  useEffect(() => {
    if (successSave) {
      window.location.pathname = '/packs'
    }

  }, [successSave])

  const submitHandler = (data) => {

    const bodyFormData = new FormData();
    bodyFormData.append('image', data.file[0]);
    bodyFormData.append('price', data.price);

    bodyFormData.append('name', data.name);

    bodyFormData.append('description', data.description);


    dispatch(
      savePack(bodyFormData)
    );
  };


  return (
    <>
      <div className='container'>


        <div className='row'>
          <div className="col-sm-6 offset-sm-3 mt-5">
            <h1>Add New pack</h1>
            <form onSubmit={handleSubmit(submitHandler)}>
              <label>Name</label>

              <input type="text" {...register("name", { required: true })} onChange={(e) => setName(e.target.value)} className="form-control" placeholder="name"></input>
              {errors.name && <p className='text-danger'>This field is required</p>}

              <br />
              <label>Description</label>

              <input type="text" {...register("description", { required: true })} onChange={(e) => setDescription(e.target.value)} className="form-control" placeholder="description"></input>
              {errors.description && <p className='text-danger'>This field is required</p>}

              <br />
              <label>Image</label>

              <input type="file" {...register("file", { required: true })} onChange={(e) => setFile(e.target.files[0])} className="form-control"></input>
              {errors.file && <p className='text-danger'>This field is required</p>}

              <br />
              <label>Price</label>

              <input type="text" {...register("price", { required: true })} onChange={(e) => setPrice(e.target.value)} className="form-control" placeholder="price"></input>
              {errors.price && <p className='text-danger'>This field is required</p>}

              <br />

              <br />
              <button type="submit" className="btn btn-lg btn-success"> ADD</button>
            </form>
          </div>
        </div></div>
    </>

  )

}

export default AddPackScreen