import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { signin } from '../actions/userActions';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
function SigninScreen(props) {

  const validationSchema = Yup.object().shape({

    email: Yup.string()
    .required('Email is required')
    .email(),    
    password: Yup.string()
        .required('Password is required')
        .min(6, 'Password must be at least 6 characters'),

        
});
const formOptions = { resolver: yupResolver(validationSchema) };
  const { register, handleSubmit, formState: { errors } } = useForm(formOptions);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const userSignin = useSelector(state => state.userSignin);
  const { loading, userInfo, error } = userSignin;
  const dispatch = useDispatch();
  const navigate = useNavigate();
  useEffect(() => {
    if (userInfo) {
        navigate("/")
    }
    return () => {
      // 
    };
  }, [userInfo]);

  const submitHandler = (data) => {
   // e.preventDefault();
    dispatch(signin(data.email, data.password));

  }
  return <div className="form">
    <form onSubmit= {handleSubmit(submitHandler)}>
      <ul className="form-container">
        <li>
          <h2>Sign-In</h2>
        </li>
        <li>
          {loading && <div>Loading...</div>}
          {error && <div className='text-danger'>{error}</div>}
        </li>
        <li>
          <label htmlFor="email">
            Email
          </label>
          <input type="email" name="email" id="email" onChange={(e) => setEmail(e.target.value)} {...register("email")} >
          </input>
          <div className="text-danger">{errors.email?.message}</div>
        </li>
        <li>
          <label htmlFor="password">Password</label>
          <input type="password" id="password" name="password" onChange={(e) => setPassword(e.target.value)}  {...register("password")}>
          </input>
          <div className="text-danger">{errors.password?.message}</div>
        </li>
        <li>
          <button type="submit" className="button primary">Signin</button>
        </li>
      
        <li>
        </li>
      </ul>
    </form>
  </div>
}
export default SigninScreen;