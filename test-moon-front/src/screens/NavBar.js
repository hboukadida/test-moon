
import React, { useState, useEffect } from 'react';
import { BrowserRouter, Link, useNavigate } from 'react-router-dom';
import data from '../data';
import { useParams } from "react-router-dom"
import { useSelector } from 'react-redux';

function NavBar(props) {

  const userSignin = useSelector(state => state.userSignin);
  const { userInfo } = userSignin;

  const openMenu = () => {
    document.querySelector(".sidebar").classList.add("open");
  }

  const closeMenu = () => {
    document.querySelector(".sidebar").classList.remove("open");
  }
  const navigate = useNavigate();

  const logout = () => {
    localStorage.clear();
    window.location.pathname = '/login'
  }
  return (
    <>
      <header className="header">
        <div className="brand">
          <button onClick={openMenu}>
            &#9776;
          </button>
          <Link to="/">Test-Moon</Link>
        </div>
        <div className="header-links">


          {userInfo ? (
            <a href='#' onClick={logout}>Logout</a>
          ) : (
            <>
              <Link to="/login">Sign In</Link>
              <Link to="/register">Register</Link>
            </>

          )}


        </div>
      </header>
      {userInfo ? (
        <aside className="sidebar">
          <h3>Test-Moon</h3>
          <button className="sidebar-close-button" onClick={closeMenu}>x</button>
          <ul>


            {userInfo && userInfo.role_id === 3 ? (
              <>
                <li>
                  <Link to="/" >Home</Link>
                </li>
                <li>
                  <Link to="/" >Products</Link>
                </li>
                <li>
                  <Link to="/listPackes" >Packs</Link>
                </li>
                <li>
                  <Link to="/products" >Products management</Link>
                </li>

                <li>
                  <Link to="/packs">Packs management</Link>
                </li>
                <li>
                  <Link to="/users">Users management</Link>
                </li>

              </>
            ) : userInfo && userInfo.role_id === 2 ? (
              <>
                <li>
                  <Link to="/" >Products</Link>
                </li>

                <li>
                  <Link to="/products" >Products management</Link>
                </li>

                <li>
                <Link to="/listPackes">Packs</Link>
              </li>


              </>
            ) : <>
              <li>
                <Link to="/" >Products</Link>
              </li>

              <li>
                <Link to="/listPackes">Packs</Link>
              </li>


            </>}

          </ul>
        </aside>
      ) : null}
    </>
  );
}
export default NavBar;