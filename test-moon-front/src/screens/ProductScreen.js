
import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Modal, Button, Card, Container, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { listProducts } from '../actions/productActions';
import ModalProduct from '../components/modal/Modal';

function ProductScreen(props) {

  const [modal, setModal] = useState(false);
  const [tempdata, setTempdata] = useState({});
  const productList = useSelector((state) => state.productList);
  const { products, loading, error } = productList;
  const dispatch = useDispatch();
  const [data, setData] = useState([]);
  const [filter, setFilter] = useState([]);
  const [details, setDetails] = useState({});
  useEffect(() => {
    getPacks();
    dispatch(listProducts());
    setFilter(products)

    return () => {
      //
    };
  }, []);

  async function getPacks() {
    let result = await fetch('http://127.0.0.1:8000/api/getPackAll');
    result = await result.json();
    console.log(result);
    setData(result)
  }

  const getData = (product) => {
    let tempdata = product;
    setTempdata(tempdata);

    console.log(product);
    return setModal(true);
  }


  async function getProduct(id) {
    let result = await fetch('http://127.0.0.1:8000/api/getPack/' + id);
    result = await result.json();
    setFilter(result.products)

  }


  async function getProductAll() {

    setFilter(products)

  }


  const [show, setShow] = useState(false);


  const handleShow = (product) => {
       setDetails(product);
    setShow(true)};
  const handleClose =() => setShow(false)

  return (

    <>

      {loading ? (
        <div>Loading...</div>
      ) : error ? (
        <div>{error}</div>
      ) : (

        <>

          <div className="buttons d-flex justify-content-center mb-5 pb-5 mt-5">
            <button className='btn btn-outline-dark me-2' onClick={() => getProductAll()}>All</button>

            {data.map(pack => {
              return (<>
                <button className='btn btn-outline-dark me-2' onClick={() => { getProduct(pack.id) }}>{pack.name}</button>
              </>)

            })}

          </div>
          {filter.length>0 ? (
              <div className="container">

              <div className='row'>
  
                {filter.map(product => {
  
  
                  return (
                    <>
                      <div className='col-md-3 mb-4'>
                        <div className='card h-100 text-center p-4'>
                          <img variant="top" src={"http://127.0.0.1:8000/" + product.image} alt="product" className='card-img-top' height={"250px"} />
                          <Card.Body>
                            <h5 className="card-title mb-0">{product.name}</h5>
                            <p className="card-text lead fw-bold ">
                              {product.price} DT
                            </p>
                            <button className='btn btn-lg btn-outline-dark' onClick={()=>handleShow(product)}>Details</button>
                          </Card.Body>
                        </div>
                      </div>
                    </>
                  )
                }
  
                )
  
                }
              </div>
  
            </div>
          ) : (
            <div className='row mt-2'>
            <p class="text-center"><h1>No Product Found</h1></p>
          </div>
          ) }
        


        </>
      )}


<Modal show={show}>

<Modal.Header >

  <Modal.Title>Details Product</Modal.Title>

</Modal.Header>

<Modal.Body>

  <>
  <div className='container'>
    <div className='row'>
      <div className='col-md-6 col-sm-6'>
      <img variant="top" src={"http://127.0.0.1:8000/" + details.image} alt="product" className='card-img-top' height={"250px"} />
      </div>
      <div className='col-md-6 col-sm-6 justify-content-center'>
        <span>{details.name}</span><br/>
        <span>{details.description}</span><br/>
        <span>{details.price} DT</span><br/>

      </div>

      </div> 
  </div>
  
  </>

</Modal.Body>

<Modal.Footer>

  <Button variant="secondary" onClick={handleClose}>Close</Button>

</Modal.Footer>

</Modal>      

    </>
  );
}
export default ProductScreen;