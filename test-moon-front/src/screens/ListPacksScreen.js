
import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { listPacks } from '../actions/packActions';
import { Button, Card, Container, Row, Col, Badge } from 'react-bootstrap';

function ListPacksScreen(props) {


    const packList = useSelector((state) => state.packList);
    const { packes, loading, error } = packList;
    const [data, setData] = useState([]);
    const dispatch = useDispatch();

    console.log(data)
    useEffect(() => {
        getpackes();

        return () => {
            //
        };
    }, []);


    async function getpackes() {
        let result = await fetch('http://127.0.0.1:8000/api/getPackAll');
        result = await result.json();
        setData(result)
    }

    return (
        <>

        {loading ? (
          <div>Loading...</div>
        ) : error ? (
          <div>{error}</div>
        ) : (
  
          <>
           
            <div className="container mb-5 pb-5 mt-5">
  
            <div  className='row'>
  
              {data.map(pack =>{
  
  
                return(
  <>
                <div className='col-md-3 mb-4'>
                  <div className='card h-100 text-center p-4'>
                    <img variant="top" src={"http://127.0.0.1:8000/" + pack.image} alt="product" className='card-img-top' height={"180px"} />
                    <Card.Body>
                      <h5 className="card-title mb-0">{pack.name}</h5>
                      <p className="card-text lead fw-bold ">
                        {pack.price} DT
                      </p>
                      <Link to={"/pack/"+pack.id} className='btn btn-lg btn-outline-dark'>Details</Link>
                    </Card.Body>
                  </div>
                </div>
                </>
                )
                }
  
              )
  
              }
              </div>
            </div>
            </>
        )}
         
      </>
    );
}
export default ListPacksScreen;