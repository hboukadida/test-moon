import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import {
    saveUser,
   
  } from '../actions/userActions';
  import { useForm } from "react-hook-form";

function AddUser() {
  const {
    register,
    handleSubmit,
    
    formState: { errors }
  } = useForm();

    const [name,setName]=useState("");
    const [password,setPassword]=useState("");
    const [email,setEmail]=useState("");
    const [role_id, setSelected] = useState("2");

    const userSave = useSelector((state) => state.userSave);
    const {
      loading: loadingSave,
      success: successSave,
      error: errorSave,
    } = userSave;

    function onChange(event) {

    const role_id =event.target.value;
    setSelected(role_id);
    }
    const dispatch = useDispatch();
    const navigate = useNavigate();
    useEffect(()=> {
      console.log(successSave)
        if (successSave) {
          window.location.pathname='/users'
        }
      
    },[successSave])
       async function signup(){
        let items ={name,password,email,role_id}
        console.log(items)

       
     
    
    }

    const submitHandler = (data) => {
        let items ={name,password,email,role_id}
        dispatch(
            saveUser(items)
        );
      };

    return (
        <>
        <div className='container'>


<div className='row'>
  <div className="col-sm-6 offset-sm-3 mt-5">
            <h1>Add New User</h1>
            <form onSubmit={handleSubmit(submitHandler)}>
              <label>Name</label>
            <input type="text" {...register("name", { required: true })} onChange={(e)=>setName(e.target.value)} className="form-control" placeholder="name"></input>  
            {errors.name && <p className='text-danger'>This field is required</p>}

            <br/> 
            <label>E-mail</label>

            <input type="email" {...register("email", { required: true })} onChange={(e)=>setEmail(e.target.value)} className="form-control" placeholder="email"></input>   
            {errors.email && <p className='text-danger'>This field is required</p>}

            <br/> 
            <label>Password</label>

             <input type="password" {...register("password", { required: true })} onChange={(e)=>setPassword(e.target.value)} className="form-control" placeholder="password"></input>   
             {errors.password && <p className='text-danger'>This field is required</p>}

             <br/> 
             <label>Role</label>

             <select className="form-select"   onChange={onChange}>
                <option  value="2">Agent</option>
                <option value="3">Administrator</option>
             
                </select>
             <br/> 
             <button  type="submit" className="btn  btn-lg btn-success"> ADD</button>
             </form> 
        </div>
        </div>
        </div>
        </>
       
    )

}

export default AddUser