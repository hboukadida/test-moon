
import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Button, Container, Row, Table } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { listProducts,deleteProduct } from '../actions/productActions';
import swal from 'sweetalert2';

function ProductsScreen(props) {
    const userSignin = useSelector(state => state.userSignin);
    const { userInfo } = userSignin;

    const productList = useSelector((state) => state.productList);
    const { products, loading, error } = productList;
    const dispatch = useDispatch();
    const productDelete = useSelector((state) => state.productDelete);
    const {
        loading: loadingDelete,
        success: successDelete,
        error: errorDelete,
    } = productDelete;
    useEffect(() => {
        dispatch(listProducts());

      if(successDelete){
        dispatch(listProducts());
      }
        return () => {
          //
        };
      }, [successDelete]);

 


      const deleteHandler = (item) => {

        swal.fire({
            title: 'Are you sure to delete this record?',
            showDenyButton: true,
            confirmButtonText: 'Save',
            icon:'warning'
          }).then((result) => {
            if (result.isConfirmed) {
                dispatch(deleteProduct(item.id));

                swal.fire('Saved!', '', 'success')
            } 
          })
    };
    return (
  
    <>
    
      {loading ? (
        <div>Loading...</div>
      ) : error ? (
        <div>{error}</div>
      ) : (
        <Container>
        <Row>

            <div className='mt-5 mb-5'>  <h1>List Of Product</h1>  
            {userInfo && userInfo.role_id === 3 ? (
            <Link to="/addProduct" className="btn btn-success btn-lg float-right">Add Product</Link>
            ) : null }
            </div>

        </Row>
       
       {products.length>0 ? (
            <Row >
            <Table striped bordered hover responsive>
                <thead>
                    <tr>
                        <th>#</th>
                        <th  style={{textAlign: "center"}}>Name</th>
                        
                        <th  style={{textAlign: "center"}}>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        products.map((item,key) =>
                            <tr key={item.id}>
                                <td  style={{textAlign: "center"}}>{key+1}</td>
                                <td  style={{textAlign: "center"}}>{item.name}</td>
                               
                                <td style={{textAlign: "center"}}>
                                    <Link to={"/updateProduct/"+item.id} className="btn btn-primary btn-lg float-right">Update</Link>
                                    {' '}
                                    {userInfo && userInfo.role_id === 3 ? (
                                        <>
                                    <button
                                        className="btn btn-danger btn-lg"
                                         onClick={() => {deleteHandler(item)}}
                                    >
                                        Delete
                                    </button>
                                    </>
                                    ) :
                                    null
                                    }
                                </td>
                            </tr>

                        )
                    }


                </tbody>
            </Table>
        </Row>
       ) : ( <div className='row mt-2'>
       <p class="text-center"><h1>No Product Found</h1></p>
     </div>)}
     
    </Container>
             )}
    </>
    );
}
export default ProductsScreen;