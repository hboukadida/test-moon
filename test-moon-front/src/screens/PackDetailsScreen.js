
import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Badge, Button, Card, Container, Row, Table } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { deletePack, listPacks } from '../actions/packActions';
import { useNavigate, useParams } from 'react-router-dom';

function PackDetailsScreen(props) {


  const packList = useSelector((state) => state.packList);
  const { packs, loading, error } = packList;
  const dispatch = useDispatch();
  const [data, setData] = useState([]);
  let { id } = useParams();
  const packDelete = useSelector((state) => state.packDelete);
  const {
    loading: loadingDelete,
    success: successDelete,
    error: errorDelete,
  } = packDelete;

  console.log(packs)


  useEffect(async () => {
    getProducts();


  }, [successDelete])



  async function getProducts() {
    let result = await fetch('http://127.0.0.1:8000/api/getPack/' + id);
    result = await result.json();
    setData(result.products)
  }



  return (

    <>

      {loading ? (
        <div>Loading...</div>
      ) : error ? (
        <div>{error}</div>
      ) : (

        <>
          {data.length > 0 ?(
              <div className="container mb-5 pb-5 mt-5">

              <div className='row'>
  
                {data.map(product => {
  
  
                  return (
                    <>
                      <div className='col-md-3 mb-4'>
                        <div className='card h-100 text-center p-4'>
                          <img variant="top" src={"http://127.0.0.1:8000/" + product.image} alt="product" className='card-img-top' height={"250px"} />
                          <Card.Body>
                            <h5 className="card-title mb-0">{product.name}</h5>
                            <p className="card-text lead fw-bold ">
                              {product.price} DT
                            </p>
                            <a href="#" className='btn btn-lg btn-outline-dark'>Details</a>
                          </Card.Body>
                        </div>
                      </div>
                    </>
                  )
                }
  
                )
  
                }
              </div>
            </div>
          ) : (
            <div className='row mt-2'>
            <p class="text-center"><h1>No Product Found</h1></p>
          </div>
          )}
        
        </>
      )}

    </>
  );
}
export default PackDetailsScreen;