import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { registerUser } from '../actions/userActions';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { useForm } from 'react-hook-form';

function RegisterScreen(props) {

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [rePassword, setRePassword] = useState('');
  const userRegister = useSelector(state => state.userRegister);
  const { loading, userInfo, error } = userRegister;
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
  
    if (userInfo) {
        navigate("/")
    }
    return () => {
      //
    };
  }, [userInfo]);


  const validationSchema = Yup.object().shape({
    name: Yup.string()
    .required('Name is required'),
    email: Yup.string()
    .required('Email is required')
    .email(),    
    password: Yup.string()
        .required('Password is required')
        .min(6, 'Password must be at least 6 characters'),
    confirmPassword: Yup.string()
        .required('Confirm Password is required')
        .oneOf([Yup.ref('password')], 'Passwords must match')
        
});
const formOptions = { resolver: yupResolver(validationSchema) };
const { register, handleSubmit, formState: { errors } } = useForm(formOptions);


  const submitHandler = (data) => {
    //e.preventDefault();
    dispatch(registerUser(data.name, data.email, data.password));
  }
  return <div className="form">
    <form onSubmit={handleSubmit(submitHandler)} >
      <ul className="form-container">
        <li>
          <h2>Create Account</h2>
        </li>
        <li>
          {loading && <div>Loading...</div>}
          {error && <div>{error}</div>}
        </li>
        <li>
          <label htmlFor="name">
            Name
          </label>
          <input type="name" name="name" id="name"{...register('name')}>
          </input>
          <div className="text-danger">{errors.name?.message}</div>

        </li>
        <li>
          <label htmlFor="email">
            Email
          </label>
          <input type="email" name="email" id="email" {...register('email')}>
          </input>
        </li>
        <div className="text-danger">{errors.email?.message}</div>

        <li>
          <label htmlFor="password">Password</label>
          <input type="password" id="password" name="password"  {...register('password')} >
          </input>
          <div className="text-danger">{errors.password?.message}</div>
        </li>
        <li>
          <label htmlFor="rePassword">Re-Enter Password</label>
          <input type="password" id="rePassword" name="rePassword" {...register('confirmPassword')}>
          </input>
          <div className="text-danger">{errors.confirmPassword?.message}</div>
        </li>
        <li>
          <button type="submit" className="button primary">Register</button>
        </li>
    

      </ul>
    </form>
  </div>
}
export default RegisterScreen;