
import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Button, Container, Row, Table } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { listUsers, deleteUser } from '../actions/userActions';
import swal from 'sweetalert2';
function UsersScreen(props) {


    const userList = useSelector((state) => state.userList);
    const userDelete = useSelector((state) => state.userDelete);
    const {
        loading: loadingDelete,
        success: successDelete,
        error: errorDelete,
    } = userDelete;
    const { users, loading, error } = userList;
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(listUsers());

        return () => {
            //
        };
    }, [successDelete]);

    const deleteHandler = (item) => {

        swal.fire({
            title: 'Are you sure to delete this record?',
            showDenyButton: true,
            confirmButtonText: 'Save',
            icon:'warning'
          }).then((result) => {
            if (result.isConfirmed) {
                dispatch(deleteUser(item.id));

                swal.fire('Saved!', '', 'success')
            } 
          })
    };
    

    return (

        <>

            {loading ? (
                <div>Loading...</div>
            ) : error ? (
                <div>{error}</div>
            ) : (
                <Container>
                    <Row>
                        <div className='mt-5 mb-5'>  <h1>List Of Users</h1>  <Link to="/addUser" className="btn btn-success btn-lg float-right">Add User</Link>
                        </div>

                    </Row>
                    <Row>
                        <Table striped bordered hover responsive>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th  style={{textAlign: "center"}}>Name</th>
                                    <th  style={{textAlign: "center"}}>Email</th>
                                    <th  style={{textAlign: "center"}}>Role</th>
                                    <th  style={{textAlign: "center"}}>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    users.map((item,key) =>
                                        <tr key={item.id}>
                                            <td>{key+1}</td>
                                            <td  style={{textAlign: "center"}}>{item.name}</td>
                                            <td  style={{textAlign: "center"}}>{item.email}</td>
                                            <td  style={{textAlign: "center"}}>{item.role.title}</td>
                                            <td  style={{textAlign: "center"}}>
                                            <Link to={"/updateUser/"+item.id} className="btn btn-primary btn-lg float-right">Update</Link>
{' '}
                                                <button
                                                    className="btn  btn-lg btn-danger"
                                                    onClick={() => {deleteHandler(item)}}
                                                >
                                                    Delete
                                                </button>

                                            </td>
                                        </tr>

                                    )
                                }


                            </tbody>
                        </Table>
                    </Row>
                </Container>
            )}
        </>
    );
}
export default UsersScreen;