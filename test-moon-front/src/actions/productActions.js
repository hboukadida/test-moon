import {
    PRODUCT_LIST_REQUEST,
    PRODUCT_LIST_SUCCESS,
    PRODUCT_LIST_FAIL,
    PRODUCT_SAVE_REQUEST,
    PRODUCT_SAVE_SUCCESS,
    PRODUCT_SAVE_FAIL,
    PRODUCT_DELETE_SUCCESS,
    PRODUCT_DELETE_FAIL,
    PRODUCT_DELETE_REQUEST,
  
  } from '../constants/productConstants';
  import axios from 'axios';
  const url ="http://127.0.0.1:8000"
  const listProducts = () => async (dispatch) => {
    try {
      dispatch({ type: PRODUCT_LIST_REQUEST });
      const { data } = await axios.get(url+
        '/api/getProductAll'
      );
      dispatch({ type: PRODUCT_LIST_SUCCESS, payload: data });
    } catch (error) {
      dispatch({ type: PRODUCT_LIST_FAIL, payload: error.message });
    }
  };

  const saveProduct = (product) => async (dispatch) => {
    try {
      dispatch({ type: PRODUCT_SAVE_REQUEST, payload: product });
      if (!product.get('id')) {
        const { data } = await axios.post(url+'/api/addProduct', product);
        dispatch({ type: PRODUCT_SAVE_SUCCESS, payload: data });
      } else {
       
    
        const { data } = await axios.post(url+'/api/updateProduct/'+product.get('id'), product);
        if(JSON.stringify(data.error)){
          dispatch({ type: PRODUCT_SAVE_FAIL, payload: data.error });
    
        }else{
          dispatch({ type: PRODUCT_SAVE_SUCCESS, payload: data });
        }
    
      }
    } catch (error) {
      dispatch({ type: PRODUCT_SAVE_FAIL, payload: error.message });
    }
  };

  const deleteProduct = (productId) => async (dispatch) => {
    try {
      dispatch({ type: PRODUCT_DELETE_REQUEST, payload: productId });
      const { data } = await axios.delete('/api/deleteProduct/' + productId);

      dispatch({ type: PRODUCT_DELETE_SUCCESS, payload: data, success: true });
    } catch (error) {
      dispatch({ type: PRODUCT_DELETE_FAIL, payload: error.message });
    }
  };

  


  
  export {
    listProducts,
    deleteProduct,
    saveProduct
  };