import {
  PACK_LIST_REQUEST,
  PACK_LIST_SUCCESS,
  PACK_LIST_FAIL,
 PACK_DELETE_SUCCESS,
 PACK_DELETE_FAIL,
 PACK_DELETE_REQUEST,
 PACK_SAVE_REQUEST,
 PACK_SAVE_SUCCESS,
 PACK_SAVE_FAIL,

  
  } from '../constants/packConstants';
  import axios from 'axios';
  const url ="http://127.0.0.1:8000"
  const listPacks = () => async (dispatch) => {
    try {
      dispatch({ type: PACK_LIST_REQUEST });
      const { data } = await axios.get(url+
        '/api/getPackAll'
      );
      dispatch({ type: PACK_LIST_SUCCESS, payload: data });
    } catch (error) {
      dispatch({ type: PACK_LIST_FAIL, payload: error.message });
    }
  };

  const deletePack = (packId) => async (dispatch) => {
    try {
   
      dispatch({ type: PACK_DELETE_REQUEST, payload: packId });
      const { data } = await axios.delete(url+'/api/deletePack/' + packId);
      dispatch({ type: PACK_DELETE_SUCCESS, payload: data, success: true });
    } catch (error) {
      dispatch({ type: PACK_DELETE_FAIL, payload: error.message });
    }
  };

  

  
  const savePack = (pack) => async (dispatch) => {
    try {
      dispatch({ type: PACK_SAVE_REQUEST, payload: pack });
  
      if (!pack.get('id')) {
        const { data } = await axios.post(url+'/api/addPack', pack);
        dispatch({ type: PACK_SAVE_SUCCESS, payload: data });
      } else {
        const { data } = await axios.post(url+
          '/api/updatePack/' + pack.get('id'),
          pack,
       
        );
        dispatch({ type: PACK_SAVE_SUCCESS, payload: data });
      }
    } catch (error) {
      dispatch({ type: PACK_SAVE_FAIL, payload: error.message });
    }
  };


  
  export {
    listPacks,
    deletePack,
    savePack,
   
  };