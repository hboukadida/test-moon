import Axios from "axios";
import Cookie from 'js-cookie';
import { 
    
    USER_LIST_REQUEST,USER_LIST_SUCCESS,USER_LIST_FAIL,
    USER_DELETE_SUCCESS,
    USER_DELETE_FAIL,
    USER_DELETE_REQUEST,
    USER_SAVE_REQUEST,
  USER_SAVE_SUCCESS,
  USER_SAVE_FAIL,
    USER_LOGOUT, USER_REGISTER_FAIL, USER_REGISTER_REQUEST, USER_REGISTER_SUCCESS, USER_SIGNIN_FAIL, USER_SIGNIN_REQUEST, USER_SIGNIN_SUCCESS } from "../constants/userConstants";

const url ="http://127.0.0.1:8000"

const signin = (email, password) => async (dispatch) => {
  dispatch({ type: USER_SIGNIN_REQUEST, payload: { email, password } });
  
    const { data } = await Axios.post(url+"/api/login", { email, password });
   
    if(JSON.stringify(data.error)){
      dispatch({ type: USER_SIGNIN_FAIL, payload: data.error });

    }else{
      dispatch({ type: USER_SIGNIN_SUCCESS, payload: data });
      localStorage.setItem('userInfo',JSON.stringify(data))
    }

 
   // Cookie.set('userInfo', JSON.stringify(data));
 

}

const registerUser = (name, email, password) => async (dispatch) => {
  dispatch({ type: USER_REGISTER_REQUEST, payload: { name, email, password } });

    const { data } = await Axios.post(url+"/api/register", { name, email, password });
    // dispatch({ type: USER_REGISTER_SUCCESS, payload: data });
    // // Cookie.set('userInfo', JSON.stringify(data));
    // localStorage.setItem('userInfo',JSON.stringify(data))

    if(JSON.stringify(data.error)){
      dispatch({ type: USER_REGISTER_FAIL, payload: data.error });
    }else{
      dispatch({ type: USER_REGISTER_SUCCESS, payload: data });
      localStorage.setItem('userInfo',JSON.stringify(data))
    }

 
}

const logout = () => (dispatch) => {
  Cookie.remove("userInfo");
  dispatch({ type: USER_LOGOUT })
}


const listUsers = () => async (dispatch) => {
    try {
      dispatch({ type: USER_LIST_REQUEST });
      const { data } = await Axios.get(url+
        '/api/getUserAll'
      );
      dispatch({ type: USER_LIST_SUCCESS, payload: data });
    } catch (error) {
      dispatch({ type: USER_LIST_FAIL, payload: error.message });
    }
  };

  const deleteUser = (userId) => async (dispatch, getState) => {
    try {
    //   const {
    //     userSignin: { userInfo },
    //   } = getState();
      dispatch({ type: USER_DELETE_REQUEST, payload: userId });
      const { data } = await Axios.delete(url+'/api/deleteUser/' + userId);
      dispatch({ type: USER_DELETE_SUCCESS, payload: data, success: true });
    } catch (error) {
      dispatch({ type: USER_DELETE_FAIL, payload: error.message });
    }
  };


  const saveUser = (user) => async (dispatch, getState) => {
    try {
      dispatch({ type: USER_SAVE_REQUEST, payload: user });
    //   const {
    //     userSignin: { userInfo },
    //   } = getState();
      
        // const { data } = await Axios.post('/api/addUser', user);
        // dispatch({ type: USER_SAVE_SUCCESS, payload: data });

        if (!user.id) {
          const { data } = await Axios.post(url+'/api/addUser', user);
          dispatch({ type: USER_SAVE_SUCCESS, payload: data });
        } else {
          console.log(user);
          const { data } = await Axios.post(url+
            '/api/updateUser/' + user.id,
            user,
         
          );
          dispatch({ type: USER_SAVE_SUCCESS, payload: data });
        }

   
    } catch (error) {
      dispatch({ type: USER_SAVE_FAIL, payload: error.message });
    }
  };

  
export { signin, registerUser, logout,listUsers,deleteUser ,saveUser};