import { 
    PACK_LIST_REQUEST,
    PACK_LIST_SUCCESS,
    PACK_LIST_FAIL,
    PACK_DELETE_SUCCESS,
 PACK_DELETE_FAIL,
 PACK_DELETE_REQUEST,
 PACK_SAVE_REQUEST,
 PACK_SAVE_SUCCESS,
 PACK_SAVE_FAIL,
  
  
  } from "../constants/packConstants";
  
  function packListReducer(state = { packs: [] }, action) {
      switch (action.type) {
        case PACK_LIST_REQUEST:
          return { loading: true, packs: [] };
        case PACK_LIST_SUCCESS:
          return { loading: false, packs: action.payload };
        case PACK_LIST_FAIL:
          return { loading: false, error: action.payload };
        default:
          return state;
      }
    }
  
  
    function packDeleteReducer(state = { pack: {} }, action) {
      switch (action.type) {
        case PACK_DELETE_REQUEST:
          return { loading: true };
        case PACK_DELETE_SUCCESS:
          return { loading: false, pack: action.payload, success: true };
        case PACK_DELETE_FAIL:
          return { loading: false, error: action.payload };
        default:
          return state;
      }
    }

    function packSaveReducer(state = { pack: {} }, action) {
      switch (action.type) {
        case PACK_SAVE_REQUEST:
          return { loading: true };
        case PACK_SAVE_SUCCESS:
          return { loading: false, success: true, pack: action.payload };
        case PACK_SAVE_FAIL:
          return { loading: false, error: action.payload };
        default:
          return state;
      }
    }

  
  export {
      packListReducer,
      packDeleteReducer,
      packSaveReducer
     
    };