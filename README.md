# Test-moon
Build an app with Laravel for the backend and Reactjs for the frontend that allows you to manage / see Products and Packs

## Frontend
- NodeJS 13.10.1 Alpine
- React 17.0.2


## Backend
- PHP 7.4 
- Laravel 8


## MySQL 

MySQL Version: 5.7.x

## Using the Project
1. clone the project
2. cd test-moon-backend
3. cp .env.docker.exemple .env
4. composer update
5. php artisan key:generate
6. cd test-moon-font
7. npm install 
8. Execute the following command and the Docker will build and run the containers;

```
docker-compose up 
```

### To-Do

1. docker ps  
2. docker exec -it test-moon_backend_1 bash  
3. php artisan migrate:fresh --seed
4. php artisan storage:link 

### Users
E-mail : administrator@admin.com 
password : passer

E-mail : agent@admin.com 
password : passer

E-mail : client@admin.com 
password : passer



