<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {



        User::insert([
            [
                'id'             => 1,
                'name'           => 'Agent',
                'email'          => 'agent@admin.com',
                'password'       => bcrypt('passer'),
                'role_id'        => 2,
            ],
        ]);
        User::insert([
            [
                'id'             => 2,
                'name'           => 'Client',
                'email'          => 'client@admin.com',
                'password'       => bcrypt('passer'),
                'role_id'        => 1,
            ],
        ]);
        User::insert([
            [
                'id'             => 3,
                'name'           => 'Administrator',
                'email'          => 'administrator@admin.com',
                'password'       => bcrypt('passer'),
                'role_id'        => 3,
            ],
        ]);

    }
}
