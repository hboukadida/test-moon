<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Role::insert([
            [
                'id'    => 1,
                'title' => 'Client',
                'level' => 1
            ],

            [
                'id'    => 2,
                'title' => 'Agent',
                'level' => 2
            ],

            [
                'id'    => 3,
                'title' => 'Administrator',
                'level' => 3
            ],

        ]);
    }
}
