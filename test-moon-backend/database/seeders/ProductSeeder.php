<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::insert([
            [
                'id'             => 1,
                'name'           => 'iPhone 12',
                'description'    => 'iphone 12',
                'image'       => 'products/iphone.jpeg',
                'price'       => '2200',
                'pack_id'=>   1
                
            ],
        ]);
        Product::insert([
            [
                'id'             => 2,
                'name'           => 'Samsung',
                'description'    => 'Samsung',
                'image'       => 'products/samsung s 21.jpeg',
                'price'       => '3200',
                'pack_id'=>   2
                
            ],
        ]);

    }
}
