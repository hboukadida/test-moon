<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pack;

class PackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pack::insert([
            [
                'id'             => 1,
                'name'           => 'iPhone',
                'description'    => 'iphone',
                'image'       => 'products/Apple.jpg',
                
            ],
        ]);
        Pack::insert([
            [
                'id'             => 2,
                'name'           => 'Samsung',
                'description'    => 'Samsung',
                'image'       => 'products/Samsung_Logo.png',
            ],
        ]);
    }
}
