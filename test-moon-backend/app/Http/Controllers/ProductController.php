<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function addProduct(Request $request)
     {
        $validator = \Validator::make($request->all(), [ 
            'name' => 'required',
            'description' => 'required',
            'image' => 'required',
            'price' => 'required',
        ]);
      
        if ($validator->fails()) {
          return response()->json($validator->errors(), 400);
        }
      
       //  dd($request->all());
        $product = new Product();
        $product->name=$request->input('name');
        $product->description=$request->input('description');
        $product->image=$request->file('image')->store("products");
        $product->price=$request->input('price');
        $product->pack_id=$request->input('pack_id')  ? $request->input('pack_id') : null;
        $product->save();
        return $product;

     }


     public function updateProduct(Request $request,$id)
     {

        $validator = \Validator::make($request->all(), [ 
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
          }
       
        $product = Product::where('id',$id)->first();
        if($product){
            $product->name=$request->input('name');
            $product->description=$request->input('description');
            $product->image=$request->file('image') ? $request->file('image')->store("products") :  $product->image ;
            $product->price=$request->input('price');
            $product->pack_id=$request->input('pack_id');
            $product->save();
            return $product;
        }else {
            return ['result'=>'Product not found'];

        }
       

     }

     public function deleteProduct($id)
     {
        $product = Product::where('id',$id)->delete();
        if($product){
            return ['result'=>'Product has been delete'];
        }else {
            return ['result'=>'Operation faild'];

        }
        

     }

     public function getProduct($id)
     {
        $product = Product::where('id',$id)->first();
        if($product){
            return $product;
        }else {
            return ['result'=>'product not found'];

        }
        

     }

     public function getProductHome()
     {
        $products = Product::orderBy('id', 'desc')->take(4)->get();
        return $products;
    }


     public function getProductAll()
     {
        $products = Product::all();
        return $products;
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
