<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Product;

class UsersController extends Controller
{


    public function register(Request $request){

        $validator = \Validator::make($request->all(), [ 
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
       
        ]);
      
        if ($validator->fails()) {
          return response()->json($validator->errors(), 400);
        }

        $user = User::where("email",$request->email)->first();
        
        if($user){
            return ['error'=>'this user alreday existe'];
        }else{

        
        $user = new User();
        $user->name=$request->input('name');
        $user->email=$request->input('email');
        $user->password=Hash::make($request->input('password'));
        $user->role_id=1;
        $user->save();
        $response = [
            'user'=>$user,
        ];
        return $response;
    }

       
    }

    public function login(Request $request)
    {


      
        $user = User::where("email",$request->email)->with('role')->first();
      
        if(!$user || !Hash::check($request->password,$user->password)){
            return ['error'=>'email or password is not matched'];
        }
        return $user;
   
    }

    public function addUser(Request $request){


        $validator = \Validator::make($request->all(), [ 
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'role_id' => 'required',
        ]);
      
        if ($validator->fails()) {
          return response()->json($validator->errors(), 400);
        }

        $user = User::where("email",$request->email)->first();
        
        if($user){
            return ['error'=>'this user alreday existe'];
        }else{

                
                $user = new User();
                $user->name=$request->input('name');
                $user->email=$request->input('email');
                $user->password=Hash::make($request->input('password'));
                $user->role_id=$request->input('role_id');;
                $user->save();
                return $user;
            }

       
    }

    public function updateUser(Request $request, $id){

        $validator = \Validator::make($request->all(), [ 
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'role_id' => 'required',
        ]);
      
        if ($validator->fails()) {
          return response()->json($validator->errors(), 400);
        }
        $user = User::where("id",$id)->first();
                $user->name=$request->input('name');
                $user->email=$request->input('email');
                $user->password= $request->input('password') ? Hash::make($request->input('password')) : $user->password;
                $user->role_id=$request->input('role_id');;
                $user->save();

                return $user;

    }

    public function deleteUser( $id){

        $user = User::where("id",$id)->delete();

        if($user){
            return ['result'=>'User has been delete'];
        }else {
            return ['result'=>'Operation faild'];

        }
        

    }


    public function getUser($id)
    {
       $user = User::where('id',$id)->with('role')->first();
       if($user){
           return $user;
       }else {
           return ['result'=>'user not found'];

       }
       

    }


    public function getUserAll()
    {
       $users = User::with('role')->get();
       return $users;
   }



    


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
