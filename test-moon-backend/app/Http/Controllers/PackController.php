<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pack;

class PackController extends Controller
{



    public function addPack(Request $request)
    {

        $validator = \Validator::make($request->all(), [ 
            'name' => 'required',
            'description' => 'required',
            'image' => 'required',
            'price' => 'required',
        ]);
      
        if ($validator->fails()) {
          return response()->json($validator->errors(), 400);
        }
       $pack = new Pack();
       $pack->name=$request->input('name');
       $pack->description=$request->input('description');
       $pack->image=$request->file('image')->store("products");
       $pack->price=$request->input('price');
       $pack->save();
       return $pack;

    }


    public function updatePack(Request $request,$id)
    {

        $validator = \Validator::make($request->all(), [ 
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
        ]);
      
        if ($validator->fails()) {
          return response()->json($validator->errors(), 400);
        }
       $pack = Pack::where('id',$id)->first();
       if($pack){
           $pack->name=$request->input('name');
           $pack->description=$request->input('description');
           $pack->image=$request->file('image') ? $request->file('image')->store("products") : $pack->image ;
           $pack->price=$request->input('price');
           $pack->save();
           return $pack;
       }else {
           return ['result'=>'pack not found'];

       }
      

    }

    public function deletePack($id)
    {
       $pack = Pack::where('id',$id)->delete();
       if($pack){
           return ['result'=>'pack has been delete'];
       }else {
           return ['result'=>'Operation faild'];

       }
       

    }

    public function getPack($id)
    {
       $pack = Pack::where('id',$id)->with('products')->first();
       if($pack){
           return $pack;
       }else {
           return ['result'=>'pack not found'];

       }
       

    }


    public function getPackAll()
    {
       $packs = Pack::all();
       return $packs;
   }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
