<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'description',"image","price","pack_id"];



    public function pack()
    {
        return $this->belongsTo(Role::class, 'pack_id');
    }
}
