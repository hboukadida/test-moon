<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PackController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware'=>['auth:sanctum']] , function () {
    // Route::get('getUserAll',[UsersController::Class,'getUserAll']);

});

Route::post('register',[UsersController::Class,'register']);
Route::post('login',[UsersController::Class,'login']);

Route::post('addUser',[UsersController::Class,'addUser']);
Route::delete('deleteUser/{id}',[UsersController::Class,'deleteUser']);
Route::get('getUser/{id}',[UsersController::Class,'getUser']);
Route::get('getUserAll',[UsersController::Class,'getUserAll']);
Route::post('updateUser/{id}',[UsersController::Class,'updateUser']);


Route::post('addProduct',[ProductController::Class,'addProduct']);
Route::delete('deleteProduct/{id}',[ProductController::Class,'deleteProduct']);
Route::get('getProduct/{id}',[ProductController::Class,'getProduct']);
Route::get('getProductAll',[ProductController::Class,'getProductAll']);
Route::post('updateProduct/{id}',[ProductController::Class,'updateProduct']);
Route::get('getProductHome',[ProductController::Class,'getProductHome']);


Route::post('addPack',[PackController::Class,'addPack']);
Route::delete('deletePack/{id}',[PackController::Class,'deletePack']);
Route::get('getPack/{id}',[PackController::Class,'getPack']);
Route::get('getPackAll',[PackController::Class,'getPackAll']);
Route::post('updatePack/{id}',[PackController::Class,'updatePack']);